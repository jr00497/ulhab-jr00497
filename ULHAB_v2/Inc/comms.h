/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		comms.h
 * Project:		ULHAB_v2
 * Date:        11 Apr 2019
 * Author:      Thom
 * Target:      
 * Copyright:   Doayee 2019
 * Description: 
 ******************************************************************************/

/* Define to prevent recursive inclusion */
#ifndef __COMMS_H_
#define __COMMS_H_

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "main.h"
#include "NMEA.h"
#include "MS5607.h"

/*******************************************************************************
 * Module definitions
 ******************************************************************************/
#define TX_PACKET_SIZE 256
#define DEST_ADR 0x7B


/*******************************************************************************
 * Module Specific Macros
 ******************************************************************************/

/*******************************************************************************
 * Module specific types
 ******************************************************************************/

typedef enum {
	PKT_TIME_FIX = (uint8_t)0,
	PKT_LAT,
	PKT_LON,
	PKT_SPD,
	PKT_ALT,
	PKT_TEMP,
	PKT_PRES,
	PKT_RAD_CNT,
	PKT_MAX,
} pkt_data_id_t;

typedef struct {
	uint32_t time : 17;
	bool fix : 1;
} pkt_time_fix_t;

typedef union {
	float f;
	uint32_t u;
	pkt_time_fix_t tf;
} pkt_payload_u;

typedef struct {
	uint8_t dummy[2];
	uint8_t adr;
	pkt_data_id_t data_id;
	pkt_payload_u payload;
	uint8_t check[2];
	uint8_t end[2];
} pkt_message_t;

typedef struct {
	pkt_payload_u time_fix;
	pkt_payload_u latitude;
	pkt_payload_u longitude;
	pkt_payload_u speed;
	pkt_payload_u altitude;
	pkt_payload_u pressure;
	pkt_payload_u temperature;
	pkt_payload_u rad_counts;
} tx_packet_t;

/*******************************************************************************
 * Module Globals
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
bool parse_nmea_into_packet(NMEA_information_t *nmea);
bool parse_ms5607_res_into_packet(MS5607_results_t *res);
bool parse_rad_count_into_packet(uint32_t count);
bool transmit_packet();

#endif /* __COMMS_H_ */
/* EOF */
