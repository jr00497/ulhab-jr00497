/*******************************************************************************
 * File:                    gps.c
 * Project:                 ULHAB GPS
 * Date:                    27/11/2018
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             GPS Interfacing
 ******************************************************************************/

#ifndef GPS_H_
#define GPS_H_

/*******************************************************************************
 * Module includes
 ******************************************************************************/
#include "main.h"
#include "string.h"

/*******************************************************************************
 * System Wide Module definitions
 ******************************************************************************/
#define GPS_UART USART1
#define DMA_RX_BUFFER_SIZE 256

/*******************************************************************************
 * Module Specific Macros
 ******************************************************************************/

/*******************************************************************************
 * Module Globals
 ******************************************************************************/

extern volatile uint8_t DMA_RX_buffer[DMA_RX_BUFFER_SIZE];
extern volatile uint8_t NMEA_string_buffer[DMA_RX_BUFFER_SIZE];
extern volatile bool NMEA_string_rdy;

/*******************************************************************************
 * Module specific types
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void HAL_UART_RxIdleCallback(UART_HandleTypeDef *huart);
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart);
void GPS_Init(void (*callback)(void));

#endif /* GPS_H_ */
