int time = 0;
int hours = 0;
int mins = 0;
int secs = 0;
int fix;
int oldFix = -1;
float lat = 0;
float lon = 0;
float speed = 0;
float alt = 0;
float pres = 0;
float temp = 0;
int rad_counts = 0;

float alt_max = 100;
float alt_min = 0;
float alt_step = 50;

float temp_max = 20;
float temp_min = 10;
float temp_step = 5;

float speed_min = 0;
float speed_max = 10;
float speed_step = 5;

float pres_max = 1500;
float pres_min = 500;
float pres_step = 100;

float rad_count_max = 50;
float rad_count_min = 0;
float rad_count_step = 50;

void CalculateMaxMins()
{
  while(alt > alt_max)
    alt_max += alt_step;
    
  while(alt < alt_min)
    alt_min -= alt_step;
    
  while(temp > temp_max)
    temp_max += temp_step;
    
  while(temp < temp_min)
    temp_min -= temp_step;
  
  while(speed > speed_max)
    speed_max += speed_step;
    
  while(speed < alt_min)
    speed_min -= speed_step;
  
  while(pres > pres_max)
    pres_max += pres_step;
    
  while(pres < alt_min)
    pres_min -= pres_step;
  
  while(rad_counts > rad_count_max)
    rad_count_max += rad_count_step;
    
  while(rad_counts < rad_count_min)
    rad_count_min -= rad_count_step;
}
