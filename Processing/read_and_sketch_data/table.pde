void FillTable()
{
  TableRow newRow = table.addRow();
  newRow.setInt("id", table.lastRowIndex());
  newRow.setString("PC time", String.format("%02d:%02d:%02d", hour(), minute(), second()));
  newRow.setInt("fix", fix);
  if(fix == 1)
    newRow.setInt("time", time);
  else 
    newRow.setInt("time", 3600*hour()+60*minute()+second());
  newRow.setInt("hours", hours);
  newRow.setInt("mins", mins);
  newRow.setInt("secs", secs);
  newRow.setFloat("altitude", alt);
  newRow.setFloat("latitude", lat);
  newRow.setFloat("longitude", lon);
  newRow.setFloat("speed", speed);
  newRow.setFloat("temperature", temp);
  newRow.setFloat("pressure", pres);
  newRow.setInt("radiation counts", rad_counts);
  
  if(logging) {
    String fileName = "ULHAB-" + str(year()) + str(month()) + str(day()) + "_" + str(logIndex) +".csv";
    saveTable(table, fileName);
  }
}

void NewTable() 
{ 
  println("New Table!");
  
  String fileName = "ULHAB-" + str(year()) + str(month()) + str(day()) + "_" + str(logIndex) +".csv";
  saveTable(table, fileName);
  
  logIndex += 1;
  
  table = new Table();
  
  table.addColumn("id");
  table.addColumn("PC time");
  table.addColumn("fix");
  table.addColumn("time");
  table.addColumn("hours");
  table.addColumn("mins");
  table.addColumn("secs");
  table.addColumn("altitude");
  table.addColumn("latitude");
  table.addColumn("longitude");
  table.addColumn("speed");
  table.addColumn("temperature");
  table.addColumn("pressure");
  table.addColumn("radiation counts");
}
