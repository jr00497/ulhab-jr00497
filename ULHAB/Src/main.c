/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		Main.c
 * Project:
 * Date:        20/02/2019 12:39:41
 * Author:      Thomas Cousins
 * Target:      STM32L4xx
 * Copyright:   Doayee 2019
 * Description: Main program body
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- System Overview ---------------------------------
 *
 * Description:
 *
 * Inputs:
 *
 * Outputs:
 *
 * Interrupts and Events:
 *
 * Main System Code:
 *
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- Relevant Files ----------------------------------
 *
 * main.h - header for this file
 *
 ******************************************************************************/

/*******************************************************************************
* Includes
******************************************************************************/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "dcmi.h"
#include "dma.h"
#include "i2c.h"
#include "quadspi.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"
#include "threads.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/

/*******************************************************************************
 * System globals
 ******************************************************************************/
extern DCMI_HandleTypeDef hDcmiHandler;

/*******************************************************************************
 * Module globals
 ******************************************************************************/
DMA2D_HandleTypeDef Dma2dHandle;
static volatile uint32_t start_the_camera_capture = 0;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
static void OnError_Handler(uint32_t condition);
static void DMA2D_Config(void);
static void TransferError(DMA2D_HandleTypeDef* );
static void TransferComplete(DMA2D_HandleTypeDef* );

/*******************************************************************************
 * Name:   int main(void)
 * Inputs: None
 * Return: Integer - but not really
 * Notes:  Runs forever, main uController program
 ******************************************************************************/
int main(void)
{

  /* Reset of all peripherals, Initialises the Flash interface and the Systick. */
  HAL_Init();

  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialise all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_USART1_UART_Init();
  MX_DCMI_Init();
  MX_QUADSPI_Init();
  MX_SPI1_Init();

  DMA2D_Config();

  BSP_CAMERA_Init(CAMERA_R320x240);

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  /* Define Threads */
  DefineThreads();

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */

  /* Infinite loop */
  while (1)
  {

  }

}

static void DMA2D_Config(void)
{
  HAL_StatusTypeDef hal_status = HAL_OK;

  /* Enable DMA2D clock */
  __HAL_RCC_DMA2D_CLK_ENABLE();

  /* NVIC configuration for DMA2D transfer complete interrupt */
  HAL_NVIC_SetPriority(DMA2D_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2D_IRQn);

  /* Configure the DMA2D Mode, Color Mode and output offset */
  Dma2dHandle.Init.Mode         = DMA2D_M2M;              /* DMA2D Mode memory to memory */
  Dma2dHandle.Init.ColorMode    = DMA2D_OUTPUT_RGB565;    /* Output color mode is RGB565 : 16 bpp */
  Dma2dHandle.Init.OutputOffset = 0x0;                    /* No offset in output */
  Dma2dHandle.Init.RedBlueSwap   = DMA2D_RB_REGULAR;      /* No R&B swap for the output image */
  Dma2dHandle.Init.AlphaInverted = DMA2D_REGULAR_ALPHA;   /* No alpha inversion for the output image */

  /* DMA2D Callbacks Configuration */
  Dma2dHandle.XferCpltCallback  = TransferComplete;
  Dma2dHandle.XferErrorCallback = TransferError;

  /* Foreground Configuration : Layer 1 */
  Dma2dHandle.LayerCfg[1].AlphaMode = DMA2D_NO_MODIF_ALPHA;
  Dma2dHandle.LayerCfg[1].InputAlpha = 0xFF;                    /* Fully opaque */
  Dma2dHandle.LayerCfg[1].InputColorMode = DMA2D_INPUT_RGB565;  /* Foreground layer format is RGB565 : 16 bpp */
  Dma2dHandle.LayerCfg[1].InputOffset = 0x0;                    /* No offset in input */
  Dma2dHandle.LayerCfg[1].RedBlueSwap   = DMA2D_RB_REGULAR;     /* No R&B swap for the input foreground image */
  Dma2dHandle.LayerCfg[1].AlphaInverted = DMA2D_REGULAR_ALPHA;  /* No alpha inversion for the input foreground image */

  Dma2dHandle.Instance = DMA2D;

  /* DMA2D Initialization */
  hal_status = HAL_DMA2D_Init(&Dma2dHandle);
  OnError_Handler(hal_status != HAL_OK);

  hal_status = HAL_DMA2D_ConfigLayer(&Dma2dHandle, 1);
  OnError_Handler(hal_status != HAL_OK);
}

/**
  * @brief  On Error Handler on condition TRUE.
  * @param  condition : Can be TRUE or FALSE
  * @retval None
  */
static void OnError_Handler(uint32_t condition)
{
  if(condition)
  {
    BSP_LED_On(LED1);
    while(1) { ; } /* Blocking on error */
  }
}

/**
  * @brief  DMA2D Transfer complete callback
  * @param  hdma2d: DMA2D handle.
  * @note   This example shows a simple way to report end of DMA2D transfer, and
  *         you can add your own implementation.
  * @retval None
  */
static void TransferComplete(DMA2D_HandleTypeDef *hdma2d)
{
  /* Set LED2 On */
  BSP_LED_On(LED2);
}

/**
  * @brief  DMA2D error callback
  * @param  hdma2d: DMA2D handle
  * @note   This example shows a simple way to report DMA2D transfer error, and you can
  *         add your own implementation.
  * @retval None
  */
static void TransferError(DMA2D_HandleTypeDef *hdma2d)
{
  /* Set LED1 On */
  BSP_LED_On(LED1);
}
/*******************************************************************************
 * Name:   void SystemClock_Config(void)                                      
 * Inputs: None                                                               
 * Return: None                                                               
 * Notes:  Configures the System Clock
 ******************************************************************************/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_11;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_MSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);

}

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
}

/*******************************************************************************
 * Name:   void SpinWait(void)                                                  
 * Inputs: none                                                                 
 * Return: none                                                                 
 * Notes:  Not a place you want to end up                                       
 * Credit: Matt Creighton                                                       
 ******************************************************************************/
void SpinWait(void)
{
  /*shucks*/
  while(1);
}

/*******************************************************************************
 * Name:   void _Error_Handler(char * file, int line)
 * Inputs: char * file - String populated with filename
 *         int line - Line number of line in file
 * Return: None                                           
 * Notes:  Default error handler from STM32Cube, redirects to SpinWait because
 *         there is no debug output implemented.     
 ******************************************************************************/
void _Error_Handler(char * file, int line)
{
  SpinWait();
}

#ifdef  USE_FULL_ASSERT
/*******************************************************************************
 * Name:   void assert_failed(uint8_t* file, uint32_t line)
 * Inputs: char * file - See above
 *         int line - See above
 * Return: None
 * Notes:  Reports assert error. Not implemented (unless you choose to).
 ******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* Not implemented */
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/* EOF */
