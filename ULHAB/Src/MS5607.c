/*******************************************************************************
 * File:                    MS5607.c
 * Project:                 ULHAB MS5607
 * Date:                    04/02/2019
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             MS5607 Interfacing
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "MS5607.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/
bool_et MS5607_ReadPROM(MS5607_t *ms5607)
{
	uint16_t buf16[8] = {0};

	/* For all 8 addresses in the PROM */
	for(uint8_t i = MS5607_PROM_ADR_MIN; i <= MS5607_PROM_ADR_MAX; i++)
	{
		uint8_t buf8[2] = {0};

		/* Send the read command for the correct prom address */
		uint8_t prom_read_command = MS5607_PROM_READ_ADR(i);
		if(HAL_I2C_Master_Transmit(	ms5607->i2c,
									MS5607_WRITE_ADR,
									&prom_read_command,
									SIZE_UINT8(1),
									MS5607_TIMEOUT) != HAL_OK) return FALSE;

		/* Read in 16 bits of data from that prom address */
		if(HAL_I2C_Master_Receive(	ms5607->i2c,
									MS5607_READ_ADR,
									buf8,
									SIZE_UINT8(2),
									MS5607_TIMEOUT) != HAL_OK) return FALSE;

		buf16[i] = (buf8[0] << 8) | buf8[1];
	}

	ms5607->prom->factory_data 	= buf16[0];
	ms5607->prom->SENS 			= buf16[1];
	ms5607->prom->OFF 			= buf16[2];
	ms5607->prom->TCS 			= buf16[3];
	ms5607->prom->TCO 			= buf16[4];
	ms5607->prom->T_ref 		= buf16[5];
	ms5607->prom->TEMPSENS 		= buf16[6];
	ms5607->prom->serial_code 	= buf16[7];

	return TRUE;
}

uint32_t MS5607_ReadTempPressure(MS5607_t *ms5607, MS5607_conversion_type_et type)
{
	/* Impossible result - we can tell it's not working */
	uint32_t ret = 0x10000000;

	/* Commands to initiate and read a conversion */
	uint8_t conversion_command = (type == MS5607_TEMP ?
									MS56007_CONVERT_D2_4096 :
									MS56007_CONVERT_D1_4096);

	uint8_t read_command = MS5607_ADC_READ;

	uint8_t buf[3] = {0};

	/* Send the conversion command */
	if(HAL_I2C_Master_Transmit(	ms5607->i2c,
								MS5607_WRITE_ADR,
								&conversion_command,
								SIZE_UINT8(1),
								MS5607_TIMEOUT) != HAL_OK) return ret;

	/* Wait for conversion to complete */
	HAL_Delay(MS5607_CONVERSION_DELAY);

	/* Send the read command */
	if(HAL_I2C_Master_Transmit(	ms5607->i2c,
								MS5607_WRITE_ADR,
								&read_command,
								SIZE_UINT8(1),
								MS5607_TIMEOUT) != HAL_OK) return ret;

	/* Read in 24 bits of data from the adc */
	if(HAL_I2C_Master_Receive(	ms5607->i2c,
								MS5607_READ_ADR,
								buf,
								SIZE_UINT8(3),
								MS5607_TIMEOUT) != HAL_OK) return ret;

	/* If it's an invalid conversion */
	if(buf[2] == 0)
		if(buf[1] == 0)
			if(buf[0] == 0)
				return ret;

	ret = (buf[0] << 16) | (buf[1] << 8) | buf[2];

	/* Return valid data */
	return (ret & MS5607_SUCCESS_MASK);
}

bool_et MS5607_TakeReading(MS5607_t *ms5607)
{
	uint32_t temp_read = MS5607_ReadTempPressure(ms5607, MS5607_TEMP);
	MS5607_ValidateResult(temp_read);

	uint32_t pressure_read = MS5607_ReadTempPressure(ms5607, MS5607_PRESSURE);
	MS5607_ValidateResult(pressure_read);

	double dT;
	double OFF;
	double SENS;
	double TEMP;
	double P;

	// calculate 1st order pressure and temperature (MS5607 1st order algorithm)
	dT=temp_read-(ms5607->prom->T_ref)*pow(2,8);
	OFF=(ms5607->prom->OFF)*pow(2,17)+dT*(ms5607->prom->TCO)/pow(2,6);
	SENS=(ms5607->prom->SENS)*pow(2,16)+dT*(ms5607->prom->TCS)/pow(2,7);
	TEMP=(2000+(dT*(ms5607->prom->TEMPSENS))/pow(2,23));

	// perform higher order corrections
	double T2=0., OFF2=0., SENS2=0.;

	if(TEMP<2000)
	{
		T2=dT*dT/pow(2,31);
		OFF2=61*(TEMP-2000)*(TEMP-2000)/pow(2,4);
		SENS2=2*(TEMP-2000)*(TEMP-2000);

		if(TEMP<-1500)
		{
			OFF2+=15*(TEMP+1500)*(TEMP+1500);
		    SENS2+=8*(TEMP+1500)*(TEMP+1500);
		}
	}

	TEMP-=T2;
	OFF-=OFF2;
	SENS-=SENS2;

	P=(((pressure_read*SENS)/pow(2,21)-OFF)/pow(2,15));


	ms5607->result->temp = (float)TEMP / 100;
	ms5607->result->pressure = (float)P / 100;

	return TRUE;
}



