/*******************************************************************************
 * File:                    Threadz.h
 * Project:
 * Date:
 * Author:                  Thomas Cousins
 * Target:                  STM32F7xx
 * Copyright:
 * Description:             Abstracts the thread control away from main
 ******************************************************************************/

#ifndef THREADS_H_
#define THREADS_H_

#include "cmsis_os.h"
#include "stdlib.h"
#include "main.h"
#include "stm32l4xx_hal.h"
#include "dcmi.h"
#include "dma.h"
#include "i2c.h"
#include "quadspi.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"
#include "threads.h"
#include "gps.h"
#include "NMEA.h"
#include "MS5607.h"

/*******************************************************************************
 * Module definitions
 ******************************************************************************/

/*******************************************************************************
 * Module Specific Macros
 ******************************************************************************/

/*******************************************************************************
 *** Module specific types
 ******************************************************************************/

/*******************************************************************************
 * Module Globals
 ******************************************************************************/
osThreadId GPSThreadHandle;
osThreadId MS5607ThreadHandle;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

void DefineThreads(void);

#endif /* THREADS_H_ */
