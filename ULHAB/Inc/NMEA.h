/*******************************************************************************
 * File:                    gps.c
 * Project:                 ULHAB GPS
 * Date:                    27/11/2018
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             GPS Interfacing
 ******************************************************************************/

#ifndef NMEA_H_
#define NMEA_H_

/*******************************************************************************
 * Module includes
 ******************************************************************************/
#include "main.h"
#include "string.h"
#include "stdlib.h"
#include "gps.h"

/*******************************************************************************
 * System Wide Module definitions
 ******************************************************************************/
#define NMEA_LEN 87

#define DTM 0x4d5444
#define GBS 0x534247
#define GGA 0x414747
#define GLL 0x4c4c47
#define GPQ 0x515047
#define GRS 0x535247
#define GSA 0x415347
#define GST 0x545347
#define GSV 0x565347
#define RMC 0x434d52
#define THS 0x534854
#define TXT 0x545854
#define VTG 0x475456
#define ZDA 0x41445a

/*******************************************************************************
 * Module Specific Macros
 ******************************************************************************/
//#define USE_LOCAL_BUFFER

/*******************************************************************************
 * Module Globals
 ******************************************************************************/

/*******************************************************************************
 * Module specific types
 ******************************************************************************/
typedef struct {
	float latitude;  	//always in north and east
	float longitude;
	float speed;		//in km/h
	float altitude;		//in m above MSL
	uint32_t time;   	//in seconds since 00:00:00
	uint8_t satelites;
	bool_et valid;
} NMEA_information_t;

typedef struct {
	uint8_t *str;
	uint8_t len;
} NMEA_string_t;

typedef struct NMEA_queue_t {
	NMEA_string_t *payload;
	struct NMEA_queue_t *next;
} NMEA_queue_t;

typedef enum {
	NMEA_SM_NONE = (uint8_t)0,
	NMEA_SM_DTM,
	NMEA_SM_GBS,
	NMEA_SM_GGA,
	NMEA_SM_GLL,
	NMEA_SM_GPQ,
	NMEA_SM_GRS,
	NMEA_SM_GSA,
	NMEA_SM_GST,
	NMEA_SM_GSV,
	NMEA_SM_RMC,
	NMEA_SM_THS,
	NMEA_SM_TXT,
	NMEA_SM_VTG,
	NMEA_SM_ZDA,
} NMEA_std_msg_et;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void NMEA_UpdateInformationFromQueue(NMEA_information_t *info, NMEA_queue_t *node);
bool_et NMEA_UpdateInformation(NMEA_information_t *info, NMEA_string_t *str);
bool_et NMEA_UpdateInformationGGA(NMEA_information_t *info, NMEA_string_t *str);
bool_et NMEA_UpdateInformationGLL(NMEA_information_t *info, NMEA_string_t *str);
NMEA_queue_t *NMEA_DecodeRawString(uint8_t *start, uint8_t *end);
void NMEA_FreeQueue(NMEA_queue_t *node);
void NMEA_FreeNode(NMEA_queue_t *node);
NMEA_queue_t *NMEA_FilterQueueOnChecksum(NMEA_queue_t *node);
NMEA_std_msg_et NMEA_DecodeMessageType(NMEA_string_t *nmea);

#endif
