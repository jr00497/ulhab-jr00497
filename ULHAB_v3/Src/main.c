/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		Main.c
 * Project:
 * Date:        11/04/2019 19:22:35
 * Author:      Thomas Cousins
 * Target:      STM32L4xx
 * Copyright:   Doayee 2019
 * Description: Main program body
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- System Overview ---------------------------------
 *
 * Description:
 *
 * Inputs:
 *
 * Outputs:
 *
 * Interrupts and Events:
 *
 * Main System Code:
 *
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- Relevant Files ----------------------------------
 *
 * main.h - header for this file
 *
 ******************************************************************************/
 

/*******************************************************************************
* Includes
******************************************************************************/
#include "main.h"
#include "stm32l4xx_hal.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "usart.h"
#include "tim.h"
#include "gpio.h"
#include "gps.h"
#include "NMEA.h"
#include "MS5607.h"
#include "comms.h"
#include "rad_counter.h"
#include "stdio.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/
 
/*******************************************************************************
 * System globals
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/
MS5607_t *ms = NULL;
bool tx_int_triggered = FALSE;
bool ms5607_int_triggered = FALSE;
bool gps_int_triggered = FALSE;
bool rad_count_int_triggered = FALSE;
uint8_t GPS_rx_num = 0;
uint8_t i = 0;
uint16_t GPS_nUpdated_num = 0;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void SystemClock_Config(void);
void GPS_ready();
void enter_Sleep(void);
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim);

/*******************************************************************************
 * Name:   int main(void)
 * Inputs: None
 * Return: Integer - but not really
 * Notes:  Runs forever, main uController program
 ******************************************************************************/
int main(void)
{

	/* Reset of all peripherals, Initialises the Flash interface and the Systick. */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialise all configured peripherals */
	MX_GPIO_Init();
	MX_DMA_Init();
	MX_USART1_UART_Init();
	MX_USART2_UART_Init();
	MX_LPUART1_UART_Init();
	MX_TIM1_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_I2C1_Init();
	MX_ADC1_Init();

	GPS_Init(GPS_ready);

	/* Initialise the MS5607 */
	ms = MS5607_Init();

	/* Take a start reading */
	if(MS5607_TakeReading(ms))
		parse_ms5607_res_into_packet(ms->result);

	HAL_TIM_Base_Start_IT(&htim1);
	HAL_TIM_Base_Start_IT(&htim2);
	HAL_TIM_Base_Start_IT(&htim3);

	uint32_t sleep_time = 0;
	uint32_t count = HAL_GetTick();

	/* Infinite loop */
	while (1)
	{
		sleep_time += HAL_GetTick() - count;

		if(ms5607_int_triggered)
		{
			if(MS5607_TakeReading(ms))
				parse_ms5607_res_into_packet(ms->result);

			ms5607_int_triggered = FALSE;
		}

		if(tx_int_triggered)
		{
			transmit_packet();

#ifdef OUTPUT_PRETTY

			uint8_t sleep_buf[64] = {0};
			uint32_t now = HAL_GetTick();
			float percentage_sleeping = 100 * (float)sleep_time / (float)now;

			sprintf((char *)sleep_buf, "Asleep %lims out of %lims uptime: %.02f%%\n\n",
					sleep_time, now, percentage_sleeping);
			HAL_UART_Transmit(&hlpuart1, sleep_buf, strlen((char *)sleep_buf), 0xFFFF);
#endif

			tx_int_triggered = FALSE;
		}

		if(gps_int_triggered)
		{
			gps_int_triggered = FALSE;

			NMEA_information_t info;

			if(NMEA_Parse_String(&info))
			{
				parse_nmea_into_packet(&info);
				GPS_nUpdated_num = 0;
			}

			GPS_nUpdated_num++;

			if(GPS_nUpdated_num > 10)
			{
				GPS_Init(GPS_ready);
				GPS_nUpdated_num = 0;
			}

			/*gps_int_triggered = FALSE;

			NMEA_information_t info[100];

			while(1)
			{
				// || GPS_rx_num++ < GPS_PERIOD
				if(!NMEA_Parse_String(&info[i]))
				{
					continue;
				}
				else
				{
					GPS_rx_num = 0;
					parse_nmea_into_packet(&info[i]);
					i++;
					break;
				}
			}*/
		}

		if(rad_count_int_triggered)
		{
			uint32_t count = rad_count();
			parse_rad_count_into_packet(count);
		}

		count = HAL_GetTick();
		enter_Sleep();
	}
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
	SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;     // don't reenter low-power mode after ISR

	if(htim == &htim1)
		rad_count_int_triggered = TRUE;

	if(htim == &htim2)
		ms5607_int_triggered = TRUE;

	if(htim == &htim3)
		tx_int_triggered = TRUE;

}

void GPS_ready()
{
	//SCB->SCR &= ~SCB_SCR_SLEEPONEXIT_Msk;     // don't reenter low-power mode after ISR
	gps_int_triggered = TRUE;
}

void enter_Sleep(void)
{
    /* Configure low-power mode */
    SCB->SCR &= ~( SCB_SCR_SLEEPDEEP_Msk );  // low-power mode = sleep mode
    SCB->SCR |= SCB_SCR_SLEEPONEXIT_Msk;     // reenter low-power mode after ISR

    /* Ensure Flash memory stays on */
    FLASH->ACR &= ~FLASH_ACR_SLEEP_PD;
    __WFI();  // enter low-power mode
}

/*******************************************************************************
 * Name:   void SystemClock_Config(void)                                      
 * Inputs: None                                                               
 * Return: None                                                               
 * Notes:  Configures the System Clock
 ******************************************************************************/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 10;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV4;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV4;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_LPUART1
                              |RCC_PERIPHCLK_I2C1|RCC_PERIPHCLK_ADC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_PCLK1;
  PeriphClkInit.AdcClockSelection = RCC_ADCCLKSOURCE_PLLSAI1;
  PeriphClkInit.PLLSAI1.PLLSAI1Source = RCC_PLLSOURCE_HSI;
  PeriphClkInit.PLLSAI1.PLLSAI1M = 1;
  PeriphClkInit.PLLSAI1.PLLSAI1N = 8;
  PeriphClkInit.PLLSAI1.PLLSAI1P = RCC_PLLP_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1Q = RCC_PLLQ_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1R = RCC_PLLR_DIV2;
  PeriphClkInit.PLLSAI1.PLLSAI1ClockOut = RCC_PLLSAI1_ADC1CLK;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

}

/*******************************************************************************
 * Name:   void SpinWait(void)                                                  
 * Inputs: none                                                                 
 * Return: none                                                                 
 * Notes:  Not a place you want to end up                                       
 * Credit: Matt Creighton                                                       
 ******************************************************************************/
void SpinWait(void)
{
  /*shucks*/
  while(1);
}

/*******************************************************************************
 * Name:   void _Error_Handler(char * file, int line)
 * Inputs: char * file - String populated with filename
 *         int line - Line number of line in file
 * Return: None                                           
 * Notes:  Default error handler from STM32Cube, redirects to SpinWait because
 *         there is no debug output implemented.     
 ******************************************************************************/
void _Error_Handler(char * file, int line)
{
  SpinWait();
}

#ifdef  USE_FULL_ASSERT
/*******************************************************************************
 * Name:   void assert_failed(uint8_t* file, uint32_t line)
 * Inputs: char * file - See above
 *         int line - See above
 * Return: None
 * Notes:  Reports assert error. Not implemented (unless you choose to).
 ******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* Not implemented */
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/* EOF */
