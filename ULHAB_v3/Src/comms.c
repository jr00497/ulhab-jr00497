/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		comms.c
 * Project:		ULHAB_v2
 * Date:        11 Apr 2019
 * Author:      Thom
 * Target:      
 * Copyright:   Doayee 2019
 * Description: 
 ******************************************************************************/

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "comms.h"
#include "NMEA.h"
#include "MS5607.h"
#include "stdio.h"
#include "math.h"
#include "usart.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/
pkt_payload_u store = {0};
uint8_t data_to_send = 0;
uint8_t pkt_count = 0;
uint16_t checksum = 0;
uint32_t bytesum = 0;

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/

bool parse_nmea_into_packet(NMEA_information_t *nmea)
{
	if(nmea == NULL) return FALSE;

	store.fix = nmea->valid;

	if(nmea->valid)
		store.time = nmea->time;
	else
		store.time = 0;

	if(nmea->altitude > 1.0)
		store.altitude = round(nmea->altitude*10);

	store.latitude = round((nmea->latitude + 90)*100000);
	store.longitude = round((nmea->longitude + 180)*100000);
	store.speed = round(nmea->speed*10);

	return TRUE;
}

bool parse_ms5607_res_into_packet(MS5607_results_t *res)
{
	if(res == NULL) return FALSE;

	store.pressure = round(res->pressure*10);
	store.temperature = round((res->temp + 60)*10);

	return TRUE;
}

bool parse_rad_count_into_packet(uint32_t count)
{
	store.rad_counts = count;

	return TRUE;
}

bool transmit_packet()
{
	uint8_t buf[sizeof(pkt_message_t)] = {0};
	pkt_message_t msg = {0};
	msg.adr = DEST_ADR;
	msg.payload = store;

	memcpy(buf, &msg, sizeof(pkt_message_t));
	for(uint8_t i = sizeof(msg.dummy) + sizeof(msg.adr); i < sizeof(pkt_message_t); i++)
	{
		buf[i] = reverse(buf[i]);
	}

	/* Find the checksum */
	bytesum = 0;
	for(uint8_t i = sizeof(msg.dummy) + sizeof(msg.adr); i < (sizeof(pkt_message_t) - sizeof(msg.check)); i+=2)
	{
		bytesum = bytesum + (buf[i]<<8) + buf[i+1];
		bytesum = bytesum + (bytesum>>16); // Add carry to bytesum (1's complement addition)
	}
	bytesum = bytesum & 0x0FFFF;
	checksum = ~bytesum;
	buf[sizeof(pkt_message_t) - 2] = (checksum & 0xFF00)>>8;
	buf[sizeof(pkt_message_t) - 1] = checksum & 0x00FF;

	HAL_UART_Transmit(&hlpuart1, buf, sizeof(pkt_message_t), 0xFFFF);

	char devmsg[200] = "";
	char temp[20] = "";
	strcat(devmsg, "Packet ");
	itoa(store.pkt_no, temp, 10);
	strcat(devmsg, temp);
	strcat(devmsg, ": ");
	for(uint8_t i = sizeof(msg.dummy); i < sizeof(pkt_message_t); i++)
	{
		itoa(buf[i], temp, 10);
		strcat(devmsg, temp);
		strcat(devmsg, ", ");
	}
	strcat(devmsg, "\n");
	HAL_UART_Transmit(&huart2, (uint8_t*) devmsg, strlen(devmsg), HAL_MAX_DELAY);

	store.pkt_no = ++pkt_count;

	return TRUE;
}

uint8_t reverse(uint8_t b) {
   b = (b & 0xF0) >> 4 | (b & 0x0F) << 4;
   b = (b & 0xCC) >> 2 | (b & 0x33) << 2;
   b = (b & 0xAA) >> 1 | (b & 0x55) << 1;
   return b;
}
