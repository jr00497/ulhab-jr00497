/*
  Software serial multple serial test

 Receives from the hardware serial, sends to software serial.
 Receives from software serial, sends to hardware serial.

 The circuit:
 * RX is digital pin 10 (connect to TX of other device)
 * TX is digital pin 11 (connect to RX of other device)

 Note:
 Not all pins on the Mega and Mega 2560 support change interrupts,
 so only the following can be used for RX:
 10, 11, 12, 13, 50, 51, 52, 53, 62, 63, 64, 65, 66, 67, 68, 69

 Not all pins on the Leonardo and Micro support change interrupts,
 so only the following can be used for RX:
 8, 9, 10, 11, 14 (MISO), 15 (SCK), 16 (MOSI).

 created back in the mists of time
 modified 25 May 2012
 by Tom Igoe
 based on Mikal Hart's example

 This example code is in the public domain.

 */
#include <SoftwareSerial.h>

#define OUTPUT_PRETTY

typedef enum {
	PKT_TIME_FIX = (uint8_t)0,
	PKT_LAT,
	PKT_LON,
	PKT_SPD,
	PKT_ALT,
	PKT_TEMP,
	PKT_PRES,
	PKT_RAD_CNT,
	PKT_MAX,
} pkt_data_id;

typedef struct {
	uint32_t time : 17;
	bool fix : 1;
} pkt_time_fix;

typedef union {
	float f;
	uint32_t u;
	pkt_time_fix tf;
} pkt_payload;

typedef struct {
	uint8_t adr;
	uint8_t data_id;
	pkt_payload payload;
	uint8_t check[2];
	uint8_t end[2];
} pkt_message;

bool fix = false;
uint32_t time = 0;
float alt = 0;
float lat = 0;
float lon = 0;
float speed = 0;
float temp = 0;
float pres = 0;
uint32_t rad_counts = 0;

uint32_t time_ref = 0;
uint32_t data_valid_time = 0;
bool data_valid = false;

SoftwareSerial mySerial(0, 1); // RX, TX

void setup() {
	// Open serial communications and wait for port to open:
	Serial.begin(9600);
	while (!Serial) {
	; // wait for serial port to connect. Needed for native USB port only
	}

	Serial.println("Starting RX");

	// set the data rate for the SoftwareSerial port
	mySerial.begin(9600);

	time_ref = data_valid_time = millis();
}

void loop() { // run over and over



	if((millis() - time_ref > 1000) && (data_valid))
	{
#ifdef OUTPUT_PRETTY
		Serial.print("Fix:         ");
		Serial.println(fix);
		Serial.print("Time:        ");
		Serial.println(time);
		Serial.print(" =>          ");
		Serial.print(time / 3600);
		Serial.print(":");
		Serial.print((time / 60) % 60);
		Serial.print(":");
		Serial.println(time % 60);
		Serial.print("Altitude:    ");
		Serial.println(alt, 2);
		Serial.print("Latitude:    ");
		Serial.println(lat, 6);
		Serial.print("Longitude:   ");
		Serial.println(lon, 6);
		Serial.print("Speed:       ");
		Serial.println(speed, 2);
		Serial.print("Temperature: ");
		Serial.println(temp, 2);
		Serial.print("Pressure:    ");
		Serial.println(pres, 1);
		Serial.print("Rad Counts:  ");
		Serial.println(rad_counts);
		Serial.println();
		Serial.println();
#else
		Serial.print(fix);
		Serial.print(",");
		Serial.print(time);
		Serial.print(",");
		Serial.print(alt, 2);
		Serial.print(",");
		Serial.print(lat, 6);
		Serial.print(",");
		Serial.print(lon, 6);
		Serial.print(",");
		Serial.print(speed, 2);
		Serial.print(",");
		Serial.print(temp, 2);
		Serial.print(",");
		Serial.print(pres, 1);
		Serial.print(",");
		Serial.print(rad_counts);
		Serial.print(",");
		Serial.println();
#endif

		time_ref = millis();
	}

	if(millis() - data_valid_time > 5000)
		data_valid = false;


	bool capture = false;
	uint8_t read = 0;
	uint8_t to_save[sizeof(pkt_message)];

	if(mySerial.available() != 0)
	{
		delay(100);

		while(mySerial.available() > 0)
		{
			char c = mySerial.read();

			if (c == 0x7B && capture == false) capture = true;

			if (capture)
			{
				if(read == 1 && c > (uint8_t)PKT_MAX)
				{
					capture = false;
					read = 0;
				}

				to_save[read++] = (uint8_t)c;

				if(read >= sizeof(pkt_message))
				{
					pkt_message message;
					memcpy(&message, to_save, sizeof(pkt_message));

					capture = false;
					read = 0;

					if(message.data_id < (uint8_t)(PKT_MAX & 0x00FF))
					{
						data_valid_time = millis();
						data_valid = true;
					}

					switch(message.data_id)
					{
						case PKT_TIME_FIX:

							time = message.payload.tf.time;
							fix = message.payload.tf.fix;

							break;

						case PKT_LAT:

							lat = message.payload.f;
							break;

						case PKT_LON:

							lon = message.payload.f;

							break;

						case PKT_SPD:

							speed = message.payload.f;

							break;

						case PKT_ALT:

							alt = message.payload.f;

							break;
						case PKT_TEMP:

							temp = message.payload.f;

							break;

						case PKT_PRES:

							pres = message.payload.f;

							break;

						case PKT_RAD_CNT:

							rad_counts = message.payload.u;

							break;
					}
				}
			}
		}
	}
}
