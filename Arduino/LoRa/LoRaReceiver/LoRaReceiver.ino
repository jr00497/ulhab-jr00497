#include <SPI.h>
#include <LoRa.h>

void setup() {
  pinMode(8, OUTPUT);

  Serial.begin(9600);
  while (!Serial);

  Serial.println("LoRa Receiver");

  if (!LoRa.begin(433E6)) {
    Serial.println("Starting LoRa failed!");
    while (1);
  }

  LoRa.setPreambleLength(8);
  LoRa.setSpreadingFactor(7);
  LoRa.setSignalBandwidth(125E3);
  LoRa.setCodingRate4(5);

}

void loop() {
  // try to parse packet
  int packetSize = LoRa.parsePacket();
  if (packetSize) {
    // received a packet
    Serial.print("RX: ");

    // read packet
    while (LoRa.available()) {
      Serial.print((uint8_t)LoRa.read(), HEX);
      Serial.print(",");
    }

    // print RSSI of packet
    Serial.print(". RSSI: ");
    Serial.println(LoRa.packetRssi());

    digitalWrite(8, HIGH);
    delay(100);
    digitalWrite(8, LOW);
  }
}
