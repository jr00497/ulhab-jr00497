###############
### Imports ###
from tkinter import*
from tkinter import messagebox
from tkinter.ttk import Progressbar
import tkinter.font
import tkinter.filedialog as fd

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.figure import Figure
from matplotlib.ticker import ScalarFormatter
import matplotlib.dates as md

import time
import serial
import array
import math
import numpy as np
from bitstruct import *
import threading
import queue

from typing import List

from datetime import datetime, date
from datetime import time as dtime
import csv

import folium
from folium import IFrame
from folium_jsbutton import JsButton
import webbrowser


###############
### Classes ###

class mainThread (threading.Thread):
    def __init__(self, data, dataQueue, cThread):
        threading.Thread.__init__(self)
        self.data = data
        self.dataQueue = dataQueue
        self.cThread = cThread
        self.boldFont =[]
        self.window = []
        self.canvas = []
        self.fig = []
        self.legendStates = []
        self.legendButtons = []
        self.receivedValueLabel = []
        self.lostValueLabel = []
        self.successValueLabel = []
        self.lastValueLabel = []
        self.captureLabel = []
        self.captureButton = []
            
    def run(self):
        
        #######################
        ### GUI definitions ###

        # Window
        self.window = Tk()
        self.window.title("ULHAB Data Logger")
        
        self.window.option_add("*Font", "Arial 11")
        frameFont = tkinter.font.Font(weight = "bold")
        titleFont = tkinter.font.Font(size = 24, weight = "bold")
        boldFont = tkinter.font.Font(size = 11, weight = "bold")
        self.boldFont = boldFont

        # Frames
        leftFrame = Frame(self.window)
        leftFrame.pack(side = LEFT, fill = BOTH, expand = True)
        leftFrame.bind("<Configure>", self.configure)
        toolbarFrame = Frame(leftFrame)
        toolbarFrame.pack(side = BOTTOM, fill = X, padx = 30, pady = (0, 30))
        rightFrame = Frame(self.window)
        rightFrame.pack(side = RIGHT, fill = X, padx = (0, 30), pady = 30)
        buttonsFrame = LabelFrame(rightFrame, text = "Data Capture", font = frameFont)
        buttonsFrame.pack(side = TOP, pady = (0, 10), fill = X)
        captureFrame = Frame(buttonsFrame)
        captureFrame.pack(side = TOP)
        saveloadFrame = Frame(buttonsFrame)
        saveloadFrame.pack(side = TOP)
        extrasFrame = LabelFrame(rightFrame, text = "Extras", font = frameFont)
        extrasFrame.pack(side = TOP, pady = (0, 10), fill = X)
        legendPacketFrame = Frame(rightFrame)
        legendPacketFrame.pack(side = TOP, pady = (0, 10), anchor = W, fill = X)
        legendFrame = LabelFrame(legendPacketFrame, text = "Legend", font = frameFont)
        legendFrame.pack(side = LEFT, anchor = W, fill = BOTH, expand = 1)
        packetFrame = LabelFrame(legendPacketFrame, text = "Packet Info", font = frameFont)
        packetFrame.pack(side = RIGHT, padx = (5, 0), fill = Y)
        imageFrame = LabelFrame(rightFrame, text = "Images", font = frameFont)
        imageFrame.pack(side = BOTTOM, fill = X)
        imageProgressFrame = Frame(imageFrame)
        imageProgressFrame.pack(anchor = W, pady = (0, 5))
        imageViewerFrame = Frame(imageFrame)
        imageViewerFrame.pack(anchor = W, pady = (5, 0))


        # Title
        date = datetime.date(datetime.now())
        dateStr = date.strftime("%d/%m/%Y")
        titleLabel = Label(leftFrame, text = "ULHAB Data - " + dateStr, font = titleFont, anchor = W, justify = LEFT)
        titleLabel.pack(side = TOP, fill = X, expand = False, padx = 30, pady = 10)


        # Graph and Toolbar
        self.fig = Figure(figsize=(8,5), dpi = 100)

        self.canvas = FigureCanvasTkAgg(self.fig, leftFrame)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side = TOP, fill = BOTH, expand = True, padx = 30)

        toolbar = NavigationToolbar2Tk(self.canvas, toolbarFrame)
        toolbar.update()


        # Record, Load, Save Buttons
        self.captureLabel = Label(captureFrame, text = "Not Capturing.", anchor = W, justify = LEFT, height = 1, width = 15)
        self.captureLabel.pack(side = RIGHT, padx = 21.5, pady = 10)

        self.captureButton = Button(captureFrame, text = 'Start Capture', bg = 'yellow green', height = 1, width = 15)
        self.captureButton.config(command = self.capturePress)
        self.captureButton.pack(side = LEFT, padx = 0, pady = 10)

        saveButton = Button(saveloadFrame, text = 'Save', command = self.savePress, bg = 'bisque2', height = 1, width = 15)
        saveButton.pack(side = LEFT, padx = 10, pady = 10)

        loadButton = Button(saveloadFrame, text = 'Load', command = self.loadPress, bg = 'bisque2', height = 1, width = 15)
        loadButton.pack(side = RIGHT, padx = 10, pady = 10)

        mapButton = Button(extrasFrame, text = 'Generate\nInteractive Map', command = self.generateMap, bg = 'bisque2', height = 2, width = 15)
        mapButton.pack(side = LEFT, padx = (20, 0), pady = 10)
        
        packetButton = Button(extrasFrame, text = 'Packet\nAnalyser', command = self.packetPress, bg = 'bisque2', height = 2, width = 15)
        packetButton.pack(side = RIGHT, padx = (0, 20), pady = 10)


        # Legend
        legendStates = []
        for i in range(7):
            state = BooleanVar()
            legendStates.append(state)
        self.legendStates = legendStates

        legendLat = Checkbutton(legendFrame, text = 'Latitude', variable = self.legendStates[0], command = self.legendCallback)
        legendLat.pack(side = TOP, anchor = W, ipady = 5)
        self.legendButtons.append(legendLat)

        legendLong = Checkbutton(legendFrame, text = 'Longitude', variable = self.legendStates[1], command = self.legendCallback)
        legendLong.pack(side = TOP, anchor = W, pady = 5)
        self.legendButtons.append(legendLong)

        legendAlt = Checkbutton(legendFrame, text = 'Altitude', variable = self.legendStates[2], command = self.legendCallback)
        legendAlt.pack(side = TOP, anchor = W, pady = 5)
        self.legendButtons.append(legendAlt)

        legendSpeed = Checkbutton(legendFrame, text = 'Speed', variable = self.legendStates[3], command = self.legendCallback)
        legendSpeed.pack(side = TOP, anchor = W, pady = 5)
        self.legendButtons.append(legendSpeed)

        legendTemp = Checkbutton(legendFrame, text = 'Temperature', variable = self.legendStates[4], command = self.legendCallback)
        legendTemp.pack(side = TOP, anchor = W, pady = 5)
        self.legendButtons.append(legendTemp)

        legendPressure = Checkbutton(legendFrame, text = 'Pressure', variable = self.legendStates[5], command = self.legendCallback)
        legendPressure.pack(side = TOP, anchor = W, pady = 5)
        self.legendButtons.append(legendPressure)

        legendRad = Checkbutton(legendFrame, text = 'Radiation', variable = self.legendStates[6], command = self.legendCallback)
        legendRad.pack(side = TOP, anchor = W, ipady = 5)
        self.legendButtons.append(legendRad)
        

        # Packet Processing UI
        receivedLabel = Label(packetFrame, text = "Packets Received:", anchor = W, justify = LEFT, height = 1, width = 15)
        receivedLabel.pack(side = TOP, anchor = W, padx = 5, ipady = 5)
        
        self.receivedValueLabel = Label(packetFrame, text = "0", height = 1, width = 15, font = boldFont)
        self.receivedValueLabel.pack(side = TOP, anchor = W, padx = 5, pady = (0, 5))
        
        lostLabel = Label(packetFrame, text = "Packets Lost:", anchor = W, justify = LEFT, height = 1, width = 15)
        lostLabel.pack(side = TOP, anchor = W, padx = 5, pady = 5)
        
        self.lostValueLabel = Label(packetFrame, text = "0",  height = 1, width = 15, font = boldFont)
        self.lostValueLabel.pack(side = TOP, anchor = W, padx = 5, pady = (0, 5))
        
        successLabel = Label(packetFrame, text = "Success Rate:", anchor = W, justify = LEFT, height = 1, width = 15)
        successLabel.pack(side = TOP, anchor = W, padx = 5, pady = 5)
        
        self.successValueLabel = Label(packetFrame, text = "N/A", height = 1, width = 15, font = boldFont)
        self.successValueLabel.pack(side = TOP, anchor = W, padx = 5, ipady = 5)
        
        lastLabel = Label(packetFrame, text = "Last Packet Time:", anchor = W, justify = LEFT, height = 1, width = 15)
        lastLabel.pack(side = TOP, anchor = W, padx = 5, pady = 5)
        
        self.lastValueLabel = Label(packetFrame, text = "N/A", height = 1, width = 15, font = boldFont)
        self.lastValueLabel.pack(side = TOP, anchor = W, padx = 5, ipady = 5)


        # Image Processing UI
        imageProgressLabel = Label(imageProgressFrame, text = "No image data\nreceived.", anchor = W, justify = LEFT, height = 2, width = 15)
        imageProgressLabel.pack(side = LEFT, anchor = W, padx = 10, pady = (10, 5))

        imageProgressbar = Progressbar(imageProgressFrame, orient = HORIZONTAL, length = 200, mode = 'determinate')
        imageProgressbar.pack(side = RIGHT, padx = (0, 10), pady = (10, 5))

        imageCountLabel = Label(imageViewerFrame, text = "0 image(s)\nprocessed.", anchor = W, justify = LEFT, height = 2, width = 15)
        imageCountLabel.pack(side = LEFT, anchor = W, padx = 10, pady = (0, 10))

        imageViewerButton = Button(imageViewerFrame, text = 'Open Image Viewer', command = self.imagePress, bg = 'bisque2', height = 1, width = 20)
        imageViewerButton.pack(side = RIGHT, padx = (0, 10), pady = (0, 10))


        # Menu Bar
        menubar = Menu(self.window)

        filemenu = Menu(menubar, tearoff = 0)
        filemenu.add_command(label = "Load", command = self.loadPress)
        filemenu.add_command(label = "Save", command = self.savePress)
        filemenu.add_separator()
        filemenu.add_command(label = "Quit", command = self.quitPress)
        menubar.add_cascade(label = "File", menu = filemenu)

        actionsmenu = Menu(menubar, tearoff = 0)
        actionsmenu.add_command(label = "Start Capture", command = self.capturePress)
        actionsmenu.add_command(label = "Packet Analyser", command = lambda: self.callback(actionsmenu, 1))
        actionsmenu.add_command(label = "Photo Viewer", command = lambda: self.callback(actionsmenu, 2))
        menubar.add_cascade(label = "Actions", menu = actionsmenu)

        helpmenu = Menu(menubar, tearoff = 0)
        helpmenu.add_command(label = "Hardware Guide", command = lambda: self.callback(helpmenu, 0))
        helpmenu.add_command(label = "Software Guide", command = lambda: self.callback(helpmenu, 1))
        helpmenu.add_command(label = "Contact Us", command = lambda: self.callback(helpmenu, 2))
        menubar.add_cascade(label = "Help", menu = helpmenu)

        self.window.config(menu = menubar) # Display the menu

        self.window.update()
        self.window.minsize(self.window.winfo_width(), self.window.winfo_height())
        
        self.checkQueue()


        #################
        ### Protocols ###

        self.window.protocol("WM_DELETE_WINDOW", self.quitPress) # Exits cleanly
        self.window.mainloop() # Loops forever
        
    def configure(self, event):
        updateFigure(self.data, self.canvas, self.fig)
        
    
    def capturePress(self):
        print("Capture button pressed.")
        if self.captureButton['text'] == "Stop Capture":
            self.captureButton.config(text = "Start Capture", bg = 'yellow green')
            self.captureLabel.config(text = "Not Capturing.")
            self.data.capturing = False
        else:
            if len(self.data.time) > 0 and self.data.saved == False:
                result = messagebox.askyesnocancel("Unsaved Data",
                                                   "Any unsaved data will be lost. Would you like to save data before continuing?",
                                                   icon = 'warning')
                if result is True:
                    print("Saving")
                    saved = self.savePress()
                    if saved:
                        self.startCapture()
                elif result is False:
                    print("Not Saving")
                    self.startCapture()
                else:
                    print("Cancelling")
            else:
                self.startCapture()
                    
                    
    def startCapture(self):
        self.captureButton.config(text = "Stop Capture", bg = 'orange red')
        self.captureLabel.config(text = "Capturing...")
        self.data.capturing = True
        self.data.reset()
        updateFigure(self.data, self.canvas, self.fig)
        self.updatePacketCount()
            
            
        
    def savePress(self):
        print("Save button pressed.")
        filename = fd.asksaveasfilename(defaultextension = ".csv")
        if (filename == "") or (filename == ()): # If cancel is pressed
            return False
        with open(filename, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerow(["Packet Number","Time","Latitude","Longitude","Altitude","Speed","Temperature","Pressure","Radiation"])
            for i in range(len(self.data.time)):
                writer.writerow([self.data.pkt_no[i], self.data.time[i], self.data.latitude[i], self.data.longitude[i], self.data.altitude[i], self.data.speed[i], self.data.temperature[i], self.data.pressure[i], self.data.rad[i]])
        self.data.saved = True
        return True


    def loadPress(self):
        print("Load button pressed.")
        if len(self.data.time) > 0 and self.data.saved == False:
            result = messagebox.askyesnocancel("Unsaved Data",
                                               "Any unsaved data will be lost. Would you like to save data before continuing?",
                                               icon = 'warning')
            if result is True:
                print("Saving")
                saved = self.savePress()
                if saved:
                    self.loadData()
            elif result is False:
                print("Not Saving")
                self.loadData()
            else:
                print("Cancelling")
        else:
            self.loadData()
            

    def loadData(self):
        if self.captureButton['text'] == "Stop Capture": # Stop capturing incoming packets
            self.captureButton.config(text = "Start Capture")
            self.captureLabel.config(text = "Not Capturing.")
            self.data.capturing = False
        path = fd.askopenfilename()
        if (path == "") or (path == ()):  # If cancel is pressed
            return False
        else:
            print(path)
            if path.lower().endswith('.csv'):
                with open(path) as csvfile:
                    try:
                        csvReader = csv.reader(csvfile, delimiter = ',')
                        first_row = next(csvReader)
                        packetNumData = []
                        timeData = []
                        latitudeData = []
                        longitudeData = []
                        altitudeData = []
                        speedData = []
                        temperatureData = []
                        pressureData = []
                        radData = []
                        for row in csvReader:
                            packetNumData.append(int(float(row[0])))
                            timeData.append(int(float(row[1])))
                            latitudeData.append(round(float(row[2]), 5))
                            longitudeData.append(round(float(row[3]), 5))
                            altitudeData.append(round(float(row[4]), 1))
                            speedData.append(round(float(row[5]), 1))
                            temperatureData.append(round(float(row[6]), 1))
                            pressureData.append(round(float(row[7]), 1))
                            radData.append(int(float(row[8])))
                        self.data.pkt_no = np.array(packetNumData, dtype = np.int32)
                        self.data.time = np.array(timeData, dtype = np.int32)
                        self.data.latitude = np.array(latitudeData, dtype = np.float32)
                        self.data.longitude = np.array(longitudeData, dtype = np.float32)
                        self.data.altitude = np.array(altitudeData, dtype = np.float32)
                        self.data.speed = np.array(speedData, dtype = np.float32)
                        self.data.temperature = np.array(temperatureData, dtype = np.float32)
                        self.data.pressure = np.array(pressureData, dtype = np.float32)
                        self.data.rad = np.array(radData, dtype = np.int16)
                    except:
                        messagebox.showerror("Error", "Could not read from file, please check file contents are in the correct format.")
                        return False
                    
                    self.data.saved = True
                    updateFigure(self.data, self.canvas, self.fig)
                    self.updatePacketCount()
                    return True
            else:
                messagebox.showerror("Error", "Incorrect file type, expected a .csv file.")
                return False
        
        
    def packetPress(self):
        print("Packet Analyser button pressed.")
        
    
    def imagePress(self):
        print("Image Viewer button pressed.")
        

    def callback(self, menu, index):
        print(menu.entrycget(index, "label"))
            
    
    def legendCallback(self):
        print('Legend states: {} {} {} {} {} {} {}' .format(self.legendStates[0].get(),
                                                            self.legendStates[1].get(),
                                                            self.legendStates[2].get(),
                                                            self.legendStates[3].get(),
                                                            self.legendStates[4].get(),
                                                            self.legendStates[5].get(),
                                                            self.legendStates[6].get()))
        
        for i in range(7):
            stateChange = self.data.legendStates[i]^self.legendStates[i].get()
            print(self.data.legendStates[i])
            print(self.legendStates[i].get())
            print(i)
            print(stateChange)
            if stateChange == True:
                stateChangeIndex = i
                print(stateChangeIndex)
                currentState = self.legendStates[i].get()
                print(currentState)
                break
        currentState = self.legendStates[stateChangeIndex].get()
        textCol = self.data.obtainData(stateChangeIndex)[2]
        if currentState == False:
            self.data.legendOrder.remove(stateChangeIndex)
            self.legendButtons[stateChangeIndex].config(font = "Arial 11", fg = 'black')
        elif currentState == True:
            self.legendButtons[stateChangeIndex].config(font = self.boldFont, fg = textCol)
            if len(self.data.legendOrder) < 4:
                self.data.addLastLegendOrder(stateChangeIndex)
            elif len(self.data.legendOrder) == 4:
                self.legendButtons[self.data.legendOrder[0]].config(font = "Arial 11", fg = 'black')
                self.legendStates[self.data.legendOrder[0]].set(False)
                self.data.removeFirstLegendOrder()
                self.data.addLastLegendOrder(stateChangeIndex)
        print(self.data.legendOrder)
        self.data.updateLegendStates(self.legendStates)
        updateFigure(self.data, self.canvas, self.fig)
        
    
    def generateMap(self):
        filename = fd.asksaveasfilename(defaultextension = ".html")
        if (filename == "") or (filename == ()): # If cancel is pressed
            return False
        try:
            folium_map = folium.Map()
                
            for i in range(len(self.data.latitude)):
                hours, mins, secs = timeToHMS(self.data.time[i])
                if hours < 10:
                    hours = '0' + str(hours)
                if mins < 10:
                    mins = '0' + str(mins)
                if secs < 10:
                    secs = '0' + str(secs)
                html="""
                    <html>
                     <body>
                      <table width="100%">
                       <tr align="left">
                        <th>Time:</th>
                        <th>{}:{}:{}</th>
                       </tr>
                       <tr>
                        <td>Packet Number:</td>
                        <td>{}</td>
                       </tr>
                       <tr>
                        <td>Latitude:</td>
                        <td>{}.{}&#176;</td>
                       </tr>
                       <tr>
                        <td>Longitude:</td>
                        <td>{}.{}&#176;</td>
                       </tr>
                       <tr>
                        <td>Altitude:</td>
                        <td>{}.{} m</td>
                       </tr>
                       <tr>
                        <td>Speed:</td>
                        <td>{}.{} km/h</td>
                       </tr>
                       <tr>
                        <td>Temperature:</td>
                        <td>{}.{} &#176;C</td>
                       </tr>
                       <tr>
                        <td>Pressure:</td>
                        <td>{}.{} mbar</td>
                       </tr>
                       <tr>
                        <td>Rad Count:</td>
                        <td>{}</td>
                       </tr>
                      </table>
                     </body>
                    </html>
                    """.format(hours, mins, secs,
                               int(self.data.pkt_no[i]),
                               int(round(self.data.latitude[i], 0)), int(round((self.data.latitude[i] % 1)*10000, 0)),
                               int(round(self.data.longitude[i], 0)), int(round((self.data.longitude[i] % 1)*10000, 0)),
                               int(round(self.data.altitude[i], 0)), int(round((self.data.altitude[i] % 1)*10, 0)),
                               int(round(self.data.speed[i], 0)), int(round((self.data.speed[i] % 1)*10, 0)),
                               int(round(self.data.temperature[i], 0)), int(round((self.data.temperature[i] % 1)*10, 0)),
                               int(round(self.data.pressure[i], 0)), int(round((self.data.pressure[i] % 1)*10, 0)),
                               int(self.data.rad[i]))
                iframe = IFrame(html = html, width = 230, height = 217)
                popup = folium.Popup(iframe)
                tooltip = folium.Tooltip("Packet Number = {}".format(int(self.data.pkt_no[i])))
                if i == 0:
                    points = [tuple([self.data.latitude[i], self.data.longitude[i]])]
                    folium.Marker(location = points[i], popup = popup, icon = folium.Icon(color='green', icon = 'circle', prefix = 'fa'), tooltip = tooltip, zIndexOffset = 1000, riseOnHover = True, riseOffset = 1000).add_to(folium_map)
                elif i == (len(self.data.latitude) - 1):
                    points += [tuple([self.data.latitude[i], self.data.longitude[i]])]
                    folium.Marker(location = points[i], popup = popup, icon = folium.Icon(color='red', icon = 'circle', prefix = 'fa'), tooltip = tooltip, riseOnHover = True, riseOffset = 1000).add_to(folium_map)
                else:
                    points += [tuple([self.data.latitude[i], self.data.longitude[i]])]
                    folium.Marker(location = points[i], popup = popup, icon = folium.Icon(icon = 'circle', prefix = 'fa'), tooltip = tooltip, riseOnHover = True, riseOffset = 1000).add_to(folium_map)
            folium.PolyLine(points, color = "red", weight = 2.5, opacity = 1).add_to(folium_map)
            folium_map.fit_bounds([[float(self.data.latitude[0]), float(self.data.longitude[0])], [float(self.data.latitude[-1]), float(self.data.longitude[-1])]])   

            JsButton(
                title='<i class="fa fa-map-marker" style="color:green"></i>',function="""
                function(btn, map) {
                    slider = L.control.slider(function(value) {alert(value);}, {id:slider, orientation: 'horizontal'}).add_to(map);
                }
                """).add_to(folium_map)


            JsButton(
                title='<i class="fa fa-map-marker" style="color:red"></i>',function="""
                function(btn, map) {
                    map.setView([51, 0],5);
                    btn.state('zoom-to-forest');
                }
                """).add_to(folium_map)
            
            folium_map.save(filename)
            webbrowser.open_new_tab(filename)
            return True
        except:
            messagebox.showerror("Error", "Could not generate map with provided data.")
            return False
        
        
    def checkQueue(self):
        while True:
            try:
                update = self.dataQueue.get(timeout=0.1)
            except:
                break
            if update == True:
                updateFigure(self.data, self.canvas, self.fig)
                self.updatePacketCount()
        self.window.after(10000, self.checkQueue)
        
        
    def updatePacketCount(self):
        print(self.data.pkt_no)
        print(self.data.time)
        if len(self.data.time) < 1:
            self.receivedValueLabel.config(text = "0")
            self.lostValueLabel.config(text = "0")
            self.successValueLabel.config(text = "N/A")
            self.lastValueLabel.config(text = "N/A")
        else:
            self.receivedValueLabel.config(text = int(len(self.data.time)))
            self.lostValueLabel.config(text = int(self.data.pkt_no[-1] - len(self.data.time)))
            successText = str(round(100*(len(self.data.time)/self.data.pkt_no[-1]), 2)) + "%"
            self.successValueLabel.config(text = successText)
            d = date.today()
            hours, mins, secs = timeToHMS(self.data.time[-1])
            t = dtime(hours, mins, secs)
            t = datetime.combine(d, t)
            timeToDisplay = t.strftime("%H:%M:%S")
            self.lastValueLabel.config(text = timeToDisplay)
        
        
        
    def quitPress(self):
        if len(self.data.time) > 0 and self.data.saved == False:
            result = messagebox.askyesnocancel("Unsaved Data",
                                               "Any unsaved data will be lost. Would you like to save data before continuing?",
                                               icon = 'warning')
            if result is True:
                print("Saving")
                saved = self.savePress()
                if saved:
                    self.closeWindow()
            elif result is False:
                print("Not Saving")
                self.closeWindow()
            else:
                print("Cancelling")
        else:
            self.closeWindow()
            
            
    def closeWindow(self):
        self.window.quit()
        self.window.destroy()
        self.cThread.capturing= False
        self.cThread.exiting = True
        


class captureThread (threading.Thread):
    def __init__(self, data, dataQueue, exiting):
        threading.Thread.__init__(self)
        self.data = data
        self.dataQueue = dataQueue
        self.exiting = exiting
      
    def run(self):
        ser = serial.Serial('/dev/ttyS0', baudrate = 9600)
        ser.flush()
        time.sleep(1)
        int_packet_data = array.array('i',(0 for i in range(22)))
        while not self.exiting:
            if ser.inWaiting() > 0:
                byte_in = int.from_bytes(ser.read(), "big")
                if byte_in == 89:
                    byte_in = int.from_bytes(ser.read(), "big")
                    if byte_in == 123:
                        int_packet_data[0] = 89;
                        int_packet_data[1] = 123;
                        bytes_packet_data = ser.read(20)
                        for i in range(20):
                            int_packet_data[i+2] = bytes_packet_data[i] # Automatically converts to int
                        payload = bytes_packet_data[0:20] # 1st inclusive, last exclusive
                        valid = checksum(payload)
                        if valid and self.data.capturing:
                            extractPayload(self.data, payload, int_packet_data)
                            self.dataQueue.put(True)



class Data:
    def __init__(self):
        self.pkt_no = np.array([])
        self.time = np.array([])
        self.latitude = np.array([])
        self.longitude = np.array([])
        self.altitude = np.array([])
        self.speed = np.array([])
        self.temperature = np.array([])
        self.pressure = np.array([])
        self.rad = np.array([])
        self.legendStates: List[bool] = [False, False, False, False, False, False, False]
        self.legendOrder: List[int] = []
        self.capturing = False
        self.firstPacketNum = 0
        self.packetNumMultiplier = 0
        self.saved = True
        
    def updateLegendStates(self, states):
        for i in range(7):
            self.legendStates[i] = states[i].get()
        
    def addLastLegendOrder(self, order):
        self.legendOrder.append(order)
    
    def removeFirstLegendOrder(self):
        self.legendOrder.pop(0)
        
    def obtainData(self, switch):
        if switch == 0:
            return self.latitude, "Latitude (°)", '#0000FF' # Blue
        elif switch == 1:
            return self.longitude, "Longitude (°)", '#FF8C00' # Dark Orange
        elif switch == 2:
            return self.altitude, "Altitude (m)", '#008B00' # Green 4
        elif switch == 3:
            return self.speed, "Speed (km/h)", '#FF0000' # Red
        elif switch == 4:
            return self.temperature, "Temperature (°C)", '#A020F0' # Purple
        elif switch == 5:
            return self.pressure, "Pressure (mbar)", '#6C7B8B' # Slate Grey 4
        elif switch == 6:
            return self.rad, "Rad Count", '#8B6508' # Dark Goldenrod 4
        else:
            return None
        
    def reset(self):
        self.pkt_no = np.array([])
        self.time = np.array([])
        self.latitude = np.array([])
        self.longitude = np.array([])
        self.altitude = np.array([])
        self.speed = np.array([])
        self.temperature = np.array([])
        self.pressure = np.array([])
        self.rad = np.array([])
        self.firstPacketNum = 0
        self.packetNumMultiplier = 0
        

#################
### Functions ###
    
def updateFigure(data, canvas, fig):
    try:
        fig.clear()
        for i in range(len(data.legendOrder)):
            ydata, ydataTitle, lineCol = data.obtainData(data.legendOrder[i])
            if len(data.legendOrder) < 2:
                ax = fig.add_subplot(1, 1, i+1)
            elif len(data.legendOrder) == 2:
                ax = fig.add_subplot(2, 1, i+1)
            else:
                ax = fig.add_subplot(2, 2, i+1)
            
            d = date.today()
            packetDateTimes = np.array([], dtype = object)
        
            if len(data.time) > 0:
                for j in range(len(data.time)):
                    hours, mins, secs = timeToHMS(data.time[j])
                    t = dtime(hours, mins, secs)
                    packetDateTimes = np.hstack((packetDateTimes, datetime.combine(d, t)))
                times = md.date2num(packetDateTimes)
                ax.plot(times, ydata, color = lineCol)
            else:
                t = dtime(0, 0, 0)
                times = md.date2num(datetime.combine(d, t))
                ax.plot(times, 0, color = lineCol) # Prevents error when legend tickboxes are selected whilst there is no stored packet data
            ax.set_title(ydataTitle)
            ax.grid(True)
            y_formatter = ScalarFormatter(useOffset = False)
            ax.yaxis.set_major_formatter(y_formatter)
            xfmt = md.DateFormatter('%H:%M:%S')
            ax.xaxis.set_major_formatter(xfmt)
            ax.tick_params(axis='x', labelrotation=45)
        fig.tight_layout()
        canvas.draw()
    except:
        updateFigure(data, canvas, fig)



def checksum(payload):
    bytesum = 0
    for i in range(0, 18, 2):
        byte1 = payload[i] << 8
        byte2 = payload[i+1]
        bytesum = bytesum + byte1 + byte2
        bytesum = bytesum + (bytesum >> 16)
    bytesum = bytesum & 0x0FFFF
    checksum_byte1 = payload[18]<<8
    checksum_byte2 = payload[19]
    checksum = checksum_byte1 + checksum_byte2
    print('bytesum = {}'.format(bytesum))
    print('checksum = {}'.format(checksum))
    total = bytesum + checksum
    if total == 65535:
        result = "Passed"
        print('Total = {} = {}\n'.format(total, result))
        return True
    else:
        result = "Failed"
        print('Total = {} = {}\n'.format(total, result))
        return False


def extractPayload(data, payload, int_packet_data):
    unpacked_payload = unpack('<u6u17b1u25u26u10u19u10u15u15', payload)
    pkt_no = unpacked_payload[0]
    print('Packet {}: {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}'
                                                                    .format(pkt_no,
                                                                    int_packet_data[0],
                                                                    int_packet_data[1],
                                                                    int_packet_data[2],
                                                                    int_packet_data[3],
                                                                    int_packet_data[4],
                                                                    int_packet_data[5],
                                                                    int_packet_data[6],
                                                                    int_packet_data[7],
                                                                    int_packet_data[8],
                                                                    int_packet_data[9],
                                                                    int_packet_data[10],
                                                                    int_packet_data[11],
                                                                    int_packet_data[12],
                                                                    int_packet_data[13],
                                                                    int_packet_data[14],
                                                                    int_packet_data[15],
                                                                    int_packet_data[16],
                                                                    int_packet_data[17],
                                                                    int_packet_data[18],
                                                                    int_packet_data[19],
                                                                    int_packet_data[20],
                                                                    int_packet_data[21]))
    
    
    if (len(data.pkt_no) > 0):
        if (pkt_no < (data.pkt_no[-1] + data.firstPacketNum - 64*data.packetNumMultiplier)): # If the packet number loops back to 0
            data.packetNumMultiplier = data.packetNumMultiplier + 1;
        pkt_no = int(pkt_no - data.firstPacketNum + 64*data.packetNumMultiplier)
    else:
        data.firstPacketNum = pkt_no - 1
        pkt_no = 1
    print('\nPacket Number\t = {}'.format(pkt_no))
    time = int(unpacked_payload[1])
    hours, mins, secs = timeToHMS(time)
    print('Time\t\t = {}\t = {}:{}:{}'.format(time, hours, mins, secs))
    fix = unpacked_payload[2]
    print('Fix\t\t = {}'.format(fix))
    lat = unpacked_payload[3]
    decoded_lat = round(lat/100000 - 90, 5)
    print('Latitude\t = {}\t = {}\N{DEGREE SIGN}'.format(lat, decoded_lat))
    lon = unpacked_payload[4]
    decoded_lon = round(lon/100000 -180, 5)
    print('Longitude\t = {}\t = {}\N{DEGREE SIGN}'.format(lon, decoded_lon))
    spd = unpacked_payload[5]
    decoded_spd = spd/10
    print('Speed\t\t = {}\t\t = {}km/h'.format(spd, decoded_spd))
    alt = unpacked_payload[6]
    decoded_alt = alt/10
    print('Altitude\t = {}\t\t = {}m'.format(alt, decoded_alt))
    temp = unpacked_payload[7]
    decoded_temp = round(temp/10 - 60, 1)
    print('Temperature\t = {}\t\t = {}\N{DEGREE SIGN}C'.format(temp, decoded_temp))
    pres = unpacked_payload[8]
    decoded_pres = pres/10
    print('Pressure\t = {}\t = {}mbar'.format(pres, decoded_pres))
    rad = int(unpacked_payload[9])
    print('Rad Count\t = {}\n\n'.format(rad))
    
    data.pkt_no = np.hstack((data.pkt_no, np.array(pkt_no)))
    data.time = np.hstack((data.time, np.array(time)))
    data.latitude = np.hstack((data.latitude, np.array(decoded_lat)))
    data.longitude = np.hstack((data.longitude, np.array(decoded_lon)))
    data.altitude = np.hstack((data.altitude, np.array(decoded_alt)))
    data.speed = np.hstack((data.speed, np.array(decoded_spd)))
    data.temperature = np.hstack((data.temperature, np.array(decoded_temp)))
    data.pressure = np.hstack((data.pressure, np.array(decoded_pres)))
    data.rad = np.hstack((data.rad, np.array(rad)))
    
    data.saved = False
   
   
def timeToHMS(time):
    hours = math.floor(time/3600)
    mins = math.floor((time - hours*3600)/60)
    secs = math.floor(time - hours*3600 - mins*60)
    return hours, mins, secs
    
    
    
##########################
### Initialize Threads ###

ULHABdata = Data()
dataQueue = queue.Queue(maxsize=0)

cThread = captureThread(ULHABdata, dataQueue, False) 
cThread.start()
mThread = mainThread(ULHABdata, dataQueue, cThread) 
mThread.start()

# Join threads so that both threads are ended when the program exits
mThread.join()
cThread.join()

