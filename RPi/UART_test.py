import time
import serial
import array
import RPi.GPIO as GPIO
from struct import *
GPIO.setmode(GPIO.BCM)

RSSIpin = 0
GPIO.setup(RSSIpin, GPIO.IN)

print('Starting program')

ser = serial.Serial('/dev/ttyS0', baudrate = 9600)
ser.flush()

time.sleep(5)

packet_num = 0
int_packet_data = array.array('i',(0 for i in range(10)))
data_id = -1
payload = array.array('i',(0 for i in range(4)))
checksum = array.array('i',(0 for i in range(2)))

def extractPayload(data_id, payload, packet_num):
    if data_id == 0: # PKT_TIME_FIX
        #fix = bit 23, time = bit 24 & byte 2 & byte 1
        time = bytearray()
        time_fix_bits = bin(payload[2])
        tf_len = len(time_fix_bits)
        if tf_len <= 4:
            if tf_len == 3:
                fix = 0
                time_msb = bytes([int(time_fix_bits[2])])
            else:
                fix = time_fix_bits[2]
                time_msb = bytes([int(time_fix_bits[3])])
        else:
            return 0
        time.extend(b'\x00')
        time.extend(time_msb)
        time.extend(bytes([int(payload[1])]))
        time.extend(bytes([int(payload[0])]))
        time = int.from_bytes(time, byteorder='big', signed=False)
        print('Packet {}: Time = {}, Fix = {}'.format(packet_num, time, fix))
        return 1
    elif data_id == 1: # PKT_LAT
        lat = unpack('f', payload)[0] # Converts to tuple, take from 1st element
        print('Packet {}: Latitude = {}'.format(packet_num, lat))
    elif data_id == 2: # PKT_LON
        lon = unpack('f', payload)[0]
        print('Packet {}: Longitude = {}'.format(packet_num, lon))
    elif data_id == 3: # PKT_SPD
        spd = unpack('f', payload)[0]
        print('Packet {}: Speed = {}'.format(packet_num, spd))
    elif data_id == 4: # PKT_ALT
        alt = unpack('f', payload)[0]
        print('Packet {}: Altitude = {}'.format(packet_num, alt))
    elif data_id == 5: # PKT_TEMP
        temp = unpack('f', payload)[0]
        print('Packet {}: Temperature = {}'.format(packet_num, temp))
    elif data_id == 6: # PKT_PRES
        pres = unpack('f', payload)[0]
        print('Packet {}: Pressure = {}'.format(packet_num, pres))
    elif data_id == 7: # PKT_RAD_CNT
        rad = unpack('I', payload)[0]
        print('Packet {}: Rad Count = {}'.format(packet_num, rad))
    elif data_id == 8: # PKT_MAX
        max_payload = unpack('I', payload)[0]
        print('Packet {}: Max = {}'.format(packet_num, max_payload))
    else:
        print(data_id)

while True:
    if ser.inWaiting() > 0:
        data = int.from_bytes(ser.read(), "big")
        if data == 123:
            int_packet_data[0] = 123;
            bytes_packet_data = ser.read(9)
            for i in range(9):
                int_packet_data[i+1] = bytes_packet_data[i] # Automatically converts to int
            if int_packet_data[9] == 123:
#                 print('Packet {}: {}, {}, {}, {}, {}, {}, {}, {}, {}, {}'.format(packet_num,
#                                                                                  int_packet_data[0],
#                                                                                  int_packet_data[1],
#                                                                                  int_packet_data[2],
#                                                                                  int_packet_data[3],
#                                                                                  int_packet_data[4],
#                                                                                  int_packet_data[5],
#                                                                                  int_packet_data[6],
#                                                                                  int_packet_data[7],
#                                                                                  int_packet_data[8],
#                                                                                  int_packet_data[9]))
                packet_num = packet_num + 1
                data_id = int_packet_data[1]
                payload = bytes_packet_data[1:5] # 1st inclusive, last exclusive
                checksum = int_packet_data[6:7]
                extractPayload(data_id, payload, packet_num)
                
            