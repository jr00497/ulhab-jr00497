import time
import serial
import array
import math
import numpy as np
import RPi.GPIO as GPIO
from bitstruct import *
GPIO.setmode(GPIO.BCM)

RSSIpin = 0
GPIO.setup(RSSIpin, GPIO.IN)

print('Starting program')

ser = serial.Serial('/dev/ttyS0', baudrate = 9600)
ser.flush()

time.sleep(5)
int_packet_data = array.array('i',(0 for i in range(22)))

def checksum(payload):
    bytesum = 0
    for i in range(0, 18, 2):
        byte1 = payload[i]<<8
        byte2 = payload[i+1]
        bytesum = bytesum + byte1 + byte2
        bytesum = bytesum + (bytesum>>16)
    bytesum = bytesum & 0x0FFFF
    checksum_byte1 = payload[18]<<8
    checksum_byte2 = payload[19]
    checksum = checksum_byte1 + checksum_byte2
    print('bytesum = {}'.format(bytesum))
    print('checksum = {}'.format(checksum))
    total = bytesum + checksum
    if total == 65535:
        result = "Passed"
        print('Total = {} = {}\n'.format(total, result))
        return True
    else:
        result = "Failed"
        print('Total = {} = {}\n'.format(total, result))
        return False

def extractPayload(payload, int_packet_data):
    unpacked_payload = unpack('<u6u17b1u25u26u10u19u10u15u15', payload)
    pkt_no = unpacked_payload[0]
    print('Packet {}: {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}'
                                                                    .format(pkt_no,
                                                                    int_packet_data[0],
                                                                    int_packet_data[1],
                                                                    int_packet_data[2],
                                                                    int_packet_data[3],
                                                                    int_packet_data[4],
                                                                    int_packet_data[5],
                                                                    int_packet_data[6],
                                                                    int_packet_data[7],
                                                                    int_packet_data[8],
                                                                    int_packet_data[9],
                                                                    int_packet_data[10],
                                                                    int_packet_data[11],
                                                                    int_packet_data[12],
                                                                    int_packet_data[13],
                                                                    int_packet_data[14],
                                                                    int_packet_data[15],
                                                                    int_packet_data[16],
                                                                    int_packet_data[17],
                                                                    int_packet_data[18],
                                                                    int_packet_data[19],
                                                                    int_packet_data[20],
                                                                    int_packet_data[21]))
    print('\nPacket Number\t = {}'.format(pkt_no))
    time = unpacked_payload[1]
    hours = math.floor(time/3600)
    mins = math.floor((time - hours*3600)/60)
    secs = math.floor(time - hours*3600 - mins*60)
    print('Time\t\t = {}\t = {}:{}:{}'.format(time, hours, mins, secs))
    fix = unpacked_payload[2]
    print('Fix\t\t = {}'.format(fix))
    lat = unpacked_payload[3]
    decoded_lat = round(lat/10000 - 90, 4)
    print('Latitude\t = {}\t = {}\N{DEGREE SIGN}'.format(lat, decoded_lat))
    lon = unpacked_payload[4]
    decoded_lon = round(lon/10000 -180, 4)
    print('Longitude\t = {}\t = {}\N{DEGREE SIGN}'.format(lon, decoded_lon))
    spd = unpacked_payload[5]
    decoded_spd = spd/10
    print('Speed\t\t = {}\t\t = {}km/h'.format(spd, decoded_spd))
    alt = unpacked_payload[6]
    decoded_alt = alt/10
    print('Altitude\t = {}\t\t = {}m'.format(alt, decoded_alt))
    temp = unpacked_payload[7]
    decoded_temp = round(temp/10 - 60, 1)
    print('Temperature\t = {}\t\t = {}\N{DEGREE SIGN}C'.format(temp, decoded_temp))
    pres = unpacked_payload[8]
    decoded_pres = pres/10
    print('Pressure\t = {}\t = {}mbar'.format(pres, decoded_pres))
    rad = unpacked_payload[9]
    print('Rad Count\t = {}\n\n'.format(rad))
    

while True:
    if ser.inWaiting() > 0:
        data = int.from_bytes(ser.read(), "big")
        if data == 89:
            data = int.from_bytes(ser.read(), "big")
            if data == 123:
                int_packet_data[0] = 89;
                int_packet_data[1] = 123;
                bytes_packet_data = ser.read(20)
                for i in range(20):
                    int_packet_data[i+2] = bytes_packet_data[i] # Automatically converts to int
                payload = bytes_packet_data[0:20] # 1st inclusive, last exclusive
                valid = checksum(payload)
                if valid:
                    extractPayload(payload, int_packet_data)
