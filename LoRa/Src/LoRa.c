/*******************************************************************************
 * File:                    LoRa.h
 * Project:                 ULHAB LoRa
 * Date:                    14/01/2019
 * Author:                  Thomas Cousins
 * Target:                  STM32L4xx
 * Copyright:               Thomas Cousins 2018
 * Description:             LoRa Interfacing
 ******************************************************************************/

//For robust compiling

/*******************************************************************************
 * Includes
 ******************************************************************************/
#include "LoRa.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/

/*******************************************************************************
 * Functions
 ******************************************************************************/
void LoRa_Begin()
{
	SX1278_hw.dio0.port = LORA_DIO0_GPIO_Port;
	SX1278_hw.dio0.pin = LORA_DIO0_Pin;
	SX1278_hw.nss.port = LORA_CS_GPIO_Port;
	SX1278_hw.nss.pin = LORA_CS_Pin;
	SX1278_hw.reset.port = LORA_RST_GPIO_Port;
	SX1278_hw.reset.pin = LORA_RST_Pin;
	SX1278_hw.spi = &hspi1;

	SX1278.hw = &SX1278_hw;

	header.src_id = LORA_SOURCE;
	header.dst_id = LORA_DEST;
	header.id = 0;

	SX1278_begin(&SX1278,
				 LORA_FREQ,
				 LORA_POWER,
				 LORA_RATE,
				 LORA_BANDWIDTH,
				 LORA_PACKET_LENGTH);
}

bool LoRa_SendPacket(LoRa_Packet_t *packet)
{
	/* Find the total size of the packet */
	size_t packet_size = 	sizeof(LoRa_Packet_Header_t) +    	//Header
							SIZE_UINT8(packet->payload_len) + 	//Payload
							SIZE_UINT8(LORA_PACKET_EXCESS);		//See LoRa.h

	/* Get a buffer to hold the packet in full */
	uint8_t *buf = malloc(packet_size);
	if(buf == NULL) return false;

	uint8_t *ptr = buf;

	/* Prepare the buffer */
	/* Header */
	memcpy((void *)ptr, (void *)&header, sizeof(LoRa_Packet_Header_t));

	/* Payload info */
	ptr += sizeof(LoRa_Packet_Header_t);
	if(ptr > buf + packet_size - 1) {
		free(buf);
		return false;
	}

	memcpy((void *)ptr, (void *)packet, SIZE_UINT8(LORA_PACKET_EXCESS - 1));

	/* Payload */
	ptr += SIZE_UINT8(LORA_PACKET_EXCESS - 1);
	if(ptr > buf + packet_size - 1) {
		free(buf);
		return false;
	}
	memcpy((void *)ptr, (void *)packet->payload, SIZE_UINT8(packet->payload_len));

	/* XOR Checksum */
	buf[packet_size-1] = 0;
	for(uint8_t i = 0; i < (uint8_t)(packet_size-1); i++)
		buf[packet_size-1] ^= buf[i];

	/* Enter the TX state */
	uint8_t ret = (uint8_t)SX1278_LoRaEntryTx(&SX1278, (uint8_t)packet_size, LORA_TIMEOUT);
	if(ret == 0) return false;

	/* Send the packet */
	ret = SX1278_LoRaTxPacket(&SX1278, (uint8_t *)buf, (uint8_t)packet_size, LORA_TIMEOUT);
	if(ret == 0) return false;

	free(buf);

	/* Increase the packet id */
	++(header.id);

	return true;
}

