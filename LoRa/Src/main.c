/*******************************************************************************
 * ------------------------------ File Info ------------------------------------
 *******************************************************************************
 * File:		Main.c
 * Project:
 * Date:        03/12/2018 15:44:52
 * Author:      Thomas Cousins
 * Target:      STM32L4xx
 * Copyright:   Doayee 2018
 * Description: Main program body
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- System Overview ---------------------------------
 *
 * Description:
 *
 * Inputs:
 *
 * Outputs:
 *
 * Interrupts and Events:
 *
 * Main System Code:
 *
 ******************************************************************************/
 
/*******************************************************************************
 * --------------------------- Relevant Files ----------------------------------
 *
 * main.h - header for this file
 *
 ******************************************************************************/
 

/*******************************************************************************
* Includes
******************************************************************************/
#include "main.h"
#include "stdio.h"
#include "stm32l4xx_hal.h"
#include "usart.h"
#include "spi.h"
#include "gpio.h"
#include "SX1278.h"
#include "LoRa.h"

/*******************************************************************************
 * Constants and definitions
 ******************************************************************************/
 
/*******************************************************************************
 * System globals
 ******************************************************************************/

/*******************************************************************************
 * Module globals
 ******************************************************************************/

/*******************************************************************************
 * Function prototypes
 ******************************************************************************/
void SystemClock_Config(void);

/*******************************************************************************
 * Name:   int main(void)
 * Inputs: None
 * Return: Integer - but not really
 * Notes:  Runs forever, main uController program
 ******************************************************************************/
int main(void)
{

  /* Reset of all peripherals, Initialises the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialise all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_LPUART1_UART_Init();
  LoRa_Begin();

  GPS_Packet_t gps;
  gps.altitude = 	71.64;
  gps.latitude = 	51.145908;
  gps.longitude = 	-0.3628544;
  gps.speed = 		0.0;
  gps.time = 		58800;

  LoRa_Packet_t lp;
  lp.payload = (void *)&gps;
  lp.payload_len = (uint8_t)sizeof(GPS_Packet_t);
  lp.type = GPS_PACKET;

  /* Infinite loop */
  while (1)
  {
	  HAL_Delay(2000);

	  bool ret = LoRa_SendPacket(&lp);

	  if(ret) HAL_GPIO_TogglePin(LED1_GPIO_Port, LED1_Pin);

  }

}

/*******************************************************************************
 * Name:   void SystemClock_Config(void)                                      
 * Inputs: None                                                               
 * Return: None                                                               
 * Notes:  Configures the System Clock
 ******************************************************************************/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_MSI;
  RCC_OscInitStruct.MSIState = RCC_MSI_ON;
  RCC_OscInitStruct.MSICalibrationValue = 0;
  RCC_OscInitStruct.MSIClockRange = RCC_MSIRANGE_6;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_MSI;
  RCC_OscInitStruct.PLL.PLLM = 1;
  RCC_OscInitStruct.PLL.PLLN = 24;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /**Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_LPUART1;
  PeriphClkInit.Lpuart1ClockSelection = RCC_LPUART1CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
  /**Configure the main internal regulator output voltage 
  */
  if (HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1) != HAL_OK)
  {
    Error_Handler();
  }

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);

}

/*******************************************************************************
 * Name:   void SpinWait(void)                                                  
 * Inputs: none                                                                 
 * Return: none                                                                 
 * Notes:  Not a place you want to end up                                       
 * Credit: Matt Creighton                                                       
 ******************************************************************************/
void SpinWait(void)
{
  /*shucks*/
  while(1);
}

/*******************************************************************************
 * Name:   void _Error_Handler(char * file, int line)
 * Inputs: char * file - String populated with filename
 *         int line - Line number of line in file
 * Return: None                                           
 * Notes:  Default error handler from STM32Cube, redirects to SpinWait because
 *         there is no debug output implemented.     
 ******************************************************************************/
void _Error_Handler(char * file, int line)
{
  SpinWait();
}

#ifdef  USE_FULL_ASSERT
/*******************************************************************************
 * Name:   void assert_failed(uint8_t* file, uint32_t line)
 * Inputs: char * file - See above
 *         int line - See above
 * Return: None
 * Notes:  Reports assert error. Not implemented (unless you choose to).
 ******************************************************************************/
void assert_failed(uint8_t* file, uint32_t line)
{
  /* Not implemented */
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/* EOF */
